import readline from "readline-sync";
import fs from "fs";

let loginStatus = false; // true: logged in, false: not logged in
let loginAccount; // currently logged in account
let allUsers; // all user account data


/**
 * Read all user account data from the file
 */
const readAccountDataFromFile = () => {
    try {
        const data = fs.readFileSync("./account_data.json", "utf8");
        return (JSON.parse(data));
    } catch (err) {
        console.log(err);
    }
};

/**
 * Save all user account data to the file
 */
const saveAccountDataToFile = () => {
    fs.writeFileSync("./account_data.json", JSON.stringify(allUsers), "utf8", (err) => {
        if (err) {
            console.log("Could not save account data to file!");
        }
    });
};

/**
 * create new ID number: maximum number of existing ID + 1
 * @returns new ID number
 */
const createNewID = () => {
    const newID = Math.max(...allUsers.map(user => user.id)) + 1;
    return newID;
}

/**
 * check the ID number if that number exist or not
 * @param {number} idNumber 
 * @returns Account information if ID number exist
 */
const checkAccountID = (idNumber) => {
    const foundAccount = allUsers.find(({ id }) => id === Number(idNumber));
    return (foundAccount);

    /*  another way to check existance of ID, returns true or false
    const userIDs = allUsers.map(account => {
        return account.id;
    })
    const exist2 = userIDs.includes(Number(answer));
    return (exist2);
    */
}

/**
 * Check if entered password is correct, if not then ask password again
 * @param {array} account 
 * @returns true
 */
const passwordEntryAndCheck = (account) => {
    let password = readline.question("Okey, we found an account with that ID. You will need to insert your password so we can validate it's actually you.\n");

    while (account.password !== password) { // When the password does not match
        password = readline.question("Ah, there must be a typo. Try typing it again.\n");
    }
    return true;
}

/**
 * Check if account exists, then check password
 * @returns account's information 
 */
const checkAccountAndPassword = () => {
    let idNumber = readline.question("What is your account ID?\n");
    let existID = checkAccountID(idNumber);
    while (existID === undefined) { // When the number cannot be found.
        idNumber = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?\n");
        existID = checkAccountID(Number(idNumber));
    }
    // When the number is found in the all user account, chceck password.
    let result = passwordEntryAndCheck(existID);
    if (result) {
        console.log(`Awesome, we validated you ${existID.name}!`);
    }
    else {
        console.log("Sorry, something went wrong.");
        return undefined;
    };
    return existID;
}

/**
 * Change balance with the amount and operator
 * @param {number} id : ID number of changing balance
 * @param {number} amount : changing amount of the money
 * @param {string} calc : operator, + or -, to change balance
 */
const changeBalance = (id, amount, calc) => {
    const index = allUsers.findIndex(account => {
        return account.id === id;
    });
    switch (calc) {
        case "+":
            allUsers[index].balance += amount;
            break;
        case "-":
            allUsers[index].balance -= amount;
            break;
        default:
            console.log("Error.");
            break;
    }
}

export const createAccount = () => {
    allUsers = readAccountDataFromFile();
    console.log("So you want to create a new account!");
    let name = "";
    let balance = 0;
    do {
        name = readline.question("Let's start with the easy queation. What is your name?\n");
    } while (name === "");
    console.log(`Hey ${name}! It's greate to have you as a client.`);

    balance = readline.question("How much cash do you want to deposit to get started with your account? (10 euros is the minimum)\n");
    while ((balance < 10) || (isNaN(balance))) {
        balance = readline.question("Unfortunately we can't open an account for such a small account. Do you have any more cash with you?\n");
    };

    const id = createNewID();
    console.log(`Greate ${name}! You now have an acocunt (ID:${id}) with balances of ${balance}.`);
    console.log(`We're happy to have you as a customer, and we want to ensure that your money is safe with us. `);
    const password = readline.question("Give us a password, which gives only you the access to your account.\n");

    const account = {
        name: name,
        password: password,
        id: id,
        balance: Number(balance),
        fundRequests: []
    }
    allUsers.push(account);
    saveAccountDataToFile();
}

export const closeAccount = () => {
    if (loginStatus) {
        allUsers = readAccountDataFromFile();
        const answer = readline.question("So you want to close your account, huh?\nAre you sure about it?(yes/no)\n");
        switch (answer) {
            case "yes":
                console.log("Okey, sad to see you leave. We've now closed your account.");
                // Find the index number of login account from the allUsers array
                const index = allUsers.findIndex(account => {
                    return account.id === loginAccount.id;
                });
                allUsers.splice(index, 1); //delete acount from the all users array
                loginStatus = false;
                loginAccount = [];
                saveAccountDataToFile();
                break;
            case "no":
                console.log("Alright, aborting the command.");
                break;
            default:
                console.log("sorry we don't understand.")
                break;
        }
    }
    else {
        console.log("Not logged in.");
        return;
    }
}

export const doesAccountExist = () => {
    allUsers = readAccountDataFromFile();
    const idNumber = readline.question("Mhmm, you want to check if an account with an ID exists. Let's do it! Give us the ID and we'll check.\n");
    const exist = checkAccountID(idNumber);
    if (exist === undefined) { // When the number cannot be found.
        console.log("Mhmm, unfortunately an account with that ID does not exist.");
    }
    else { // When the number is found in the all user account.
        console.log("Awsome! This account actually exists. You should confirm with the owner that this account is actually his.");
    }
}

export const modifyAccount = () => {
    allUsers = readAccountDataFromFile();
    let idNumber = readline.question("Mhmm, you want to modify an account's stored holder name. We can definitely do that! Let's start validating you with your ID!\n");
    let existID = checkAccountID(idNumber);
    while (existID === undefined) { // When the number cannot be found.
        console.log("Mhmm, unfortunately an account with that ID does not exist.");
        idNumber = readline.question();
        existID = checkAccountID(idNumber);
    }
    // When the number is found in the all user account, chceck password.
    let result = passwordEntryAndCheck(existID);
    if (result) {
        let newName = readline.question(`Awesome, we validated you ${existID.name}! What is the new name for the account holder?\n`);
        while (newName === existID.name) {
            newName = readline.question("Mhmm, I'm quite sure this is the same name. Try typing it out again.\n");
        }
        console.log(`Ah, there we go. We will address you as ${newName} from now on.`);

        const index = allUsers.findIndex(account => {
            return account.id === existID.id;
        });
        allUsers[index].name = newName;
        //in case, user modifies account again and again, change LoginStatus.name as well
        if (loginStatus && (existID.id === loginAccount.id)) {
            loginAccount.name = newName;
        }
        saveAccountDataToFile();
    }
    else {
        console.log("Sorry, something went wrong.");

    }
}

export const loginCommand = () => {
    if (loginStatus) {
        console.log("Already logged in. Logout first if you wish to change the user.");
        return;
    }

    let idNumber = readline.question("So you want to log in? Give us your ID.\n");
    let existID = checkAccountID(Number(idNumber));
    while (existID === undefined) { // When the number cannot be found.
        idNumber = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?\n");
        existID = checkAccountID(Number(idNumber));
    }
    // When the number is found in the all user account, chceck password.
    let result = passwordEntryAndCheck(existID);
    if (result) {
        console.log(`Awesome, we validated you ${existID.name}! You are now logged in.`);
        loginStatus = true;
        loginAccount = existID;
    }
    else {
        console.log("Sorry, something went wrong.");
    }
}

export const logoutCommand = () => {
    if (loginStatus) {
        const answer = readline.question("Are you sure you wish to logout?\n");
        switch (answer) {
            case "yes":
                console.log("Now you are logged out.");
                loginStatus = false;
                loginAccount = [];
                break;
            case "no":
                console.log("Aborting the command.");
                break;
            default:
                console.log("Sorry I don't understand.")
                break;
        }
    }
    else {
        console.log("No user is currently logged in.");
    }
}

export const withdrawFunds = () => {
    allUsers = readAccountDataFromFile();
    console.log("Okay, let's whip up some cash for you from these ones and zeros.");

    let user;
    if (loginStatus) {
        user = loginAccount;
    }
    else {
        user = checkAccountAndPassword();
        if (user === undefined) {
            console.log("Sorry, something went wrong.");
            return;
        }
    }

    let amount = readline.question(`How much money do you want to withdraw? (Current balance: ${user.balance} euros)\n`);
    while (amount > user.balance) {
        amount = readline.question("Unfortunately you don't have the balance for that. Let's try a smaller amount.\n");
    };

    console.log(`Awesome, you can now enjoy your ${amount} euros in cash! There's still ${(user.balance - amount)} euros in your acocunt, safe with us.`);
    changeBalance(user.id, Number(amount), "-");
    saveAccountDataToFile();
}


export const depositFunds = () => {
    allUsers = readAccountDataFromFile();
    console.log("Okay, let's convert your cash into some delicious ones and zeros, then feed them into our hungry system.");

    let user;
    if (loginStatus) {
        user = loginAccount;
    }
    else {
        user = checkAccountAndPassword();
        if (user === undefined) {
            console.log("Sorry, something went wrong.");
            return;
        }
    }


    let amount = readline.question(`How much money do you want to deposit? (Current balance: ${user.balance} euros)\n`);
    while (amount < 0) {
        amount = readline.question("Unfortunately you cannot deposit negative amount. Try again.\n");
    };
    const newBalance = user.balance + Number(amount);

    console.log(`Awesome, we removed ${amount} euros from existence and stored them into our system. Now your accounts balance is ${newBalance} euros.`);

    changeBalance(user.id, Number(amount), "+");
    saveAccountDataToFile();
}

export const transferFunds = () => {
    allUsers = readAccountDataFromFile();
    console.log("Okay, let's slide these binary treats in to someone else's pockets.");

    let user;
    if (loginStatus) {
        user = loginAccount;
    }
    else {
        user = checkAccountAndPassword();
        if (user === undefined) {
            console.log("Sorry, something went wrong.");
            return;
        }
    }

    let amount = readline.question(`How much money do you want to transfer? (Current balance: ${user.balance} euros)\n`);
    while (amount > user.balance) {
        amount = readline.question("Unfortunately you don't have the balance for that. Let's try a smaller amount.\n");
    };
    console.log(`Awesome, we can do that. `);
    let payeeIDNumber = readline.question("What is the ID of the account you want to transfer these funds into?\n");
    let payee = checkAccountID(Number(payeeIDNumber));
    while (payee === undefined) { // When the number cannot be found.
        payeeIDNumber = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?\n");
        payee = checkAccountID(Number(payeeIDNumber));
    }
    // when payee's ID number is found
    console.log(`Awesome. We sent ${amount} euros to an account with the ID of ${payeeIDNumber}.`);

    changeBalance(user.id, Number(amount), "-");
    changeBalance(payee.id, Number(amount), "+");
    saveAccountDataToFile();
}

export const requestFunds = () => {
    if (loginStatus) {
        allUsers = readAccountDataFromFile();
        let requestID = readline.question("So you want request funds from someone? Give us their ID.\n");
        let requestUser = checkAccountID(Number(requestID));
        while (requestUser === undefined) { // When the number cannot be found.
            requestID = readline.question("Mhmm, unfortunately an account with that ID does not exist. Try again?\n");
            requestUser = checkAccountID(Number(requestID));
        }
        // when request user's ID number is found
        const amount = readline.question("Okay, we found an account with that ID. How much money do you want to request? \n");
        console.log(`Awesome! We'll request that amount from the user with ID ${requestID}.`);
        const index = allUsers.findIndex(account => {
            return account.id === requestUser.id;
        });
        const request = {
            id: loginAccount.id,
            amount: amount
        }
        allUsers[index].fundRequests.push(request);
        saveAccountDataToFile();
        return;
    }
    else {
        console.log("Not logged in. Login first to do this command.");
        return;
    }

}
export const fundsRequests = () => {
    if (loginStatus) {
        if (loginAccount.fundRequests.length === 0) {
            console.log("There's no request for you.");
            return;
        }
        else {
            console.log("Here's all the requests that are out for your funds.");
            loginAccount.fundRequests.map(request => {
                console.log(`${request.amount} euros for user ${request.id}.`);
            })
            return;
        }
    }
    else {
        console.log("Not logged in. Login first to do this command.");
        return;
    }

}

export const acceptFundRequest = () => {
    if (loginStatus) {
        allUsers = readAccountDataFromFile();
        const index = allUsers.findIndex(account => {
            return account.id === loginAccount.id;
        });
        if (allUsers[index].fundRequests.length === 0) {
            console.log("There's no request for you. Try it again later.");
            return;
        }
        else {
            let requestID = readline.question("So you want to accept someone's fund request? Give us their ID.\n");
            let foundRequest = allUsers[index].fundRequests.find(({ id }) => id === Number(requestID));

            while (foundRequest === undefined) { // When the request cannot be found.
                requestID = readline.question("Mhmm, unfortunately there's no request for your funds with that account ID. Try again?\n");
                foundRequest = allUsers[index].fundRequests.find(({ id }) => id === Number(requestID));
            }
            let answer = readline.question(`Okay, we found a request for your funds of ${foundRequest.amount} euros. Type yes to accept this request.`);
            switch (answer) {
                case "yes":
                    if (foundRequest.amount > allUsers[index].balance) {
                        console.log("Unfortunately you don't have the funds for a request like this!");
                        return;
                    }
                    else {
                        console.log(`Good! Now these funds has been transferred to the acocunt, with ID ${foundRequest.id}`);
                        changeBalance(loginAccount.id, Number(foundRequest.amount), "-");
                        changeBalance(foundRequest.id, Number(foundRequest.amount), "+");
                        const fundRequestIndex = allUsers[index].fundRequests.findIndex(({ id }) => id === Number(requestID));
                        allUsers[index].fundRequests.splice(fundRequestIndex, 1);
                        saveAccountDataToFile();
                    }
                    break;
                case "no":
                    break;
                default:
                    console.log("Sorry I don't understand that.");
                    break;
            }

            return;
        }
    }
    else {
        console.log("Not logged in. Login first to do this command.");
        return;
    }

}
