import readline from "readline-sync";
import fs from "fs";

import {
    createAccount,
    closeAccount,
    modifyAccount,
    doesAccountExist,
    loginCommand,
    logoutCommand,
    withdrawFunds,
    depositFunds,
    transferFunds,
    requestFunds,
    fundsRequests,
    acceptFundRequest
} from "./command.js"

/**
 * main program for the banking system
 */
const mainProgram = () => {
    let running = true;
    while (running) {
        const answer = readline.question("Welcome to Buutti banking CLI! ");
        switch (answer) {
            case "help":
                console.log(fs.readFileSync("helptext.txt", "utf-8"));
                break;
            case "createAccount":
                createAccount();
                break;
            case "closeAccount":
                closeAccount();
                break;
            case "modifyAccount":
                modifyAccount();
                break;
            case "doesAccountExist":
                doesAccountExist();
                break;
            case "login":
                loginCommand();
                break;
            case "logout":
                logoutCommand();
                break;
            case "withdrawFunds":
                withdrawFunds();
                break;
            case "depositFunds":
                depositFunds();
                break;
            case "transferFunds":
                transferFunds();
                break;
            case "requestFunds":
                requestFunds();
                break;
            case "fundsRequests", "fundRequests":
                fundsRequests();
                break;
            case "acceptFundRequest":
                acceptFundRequest();
                break;
            case "quit":
                running = false;
                break;
            default:
                console.log("Sorry, I do not understand that. \nEnter help to see the command list.");
                break;
        }
    }

    console.log();
    console.log("Bye!");
};


mainProgram();
