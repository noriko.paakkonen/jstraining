/*  Homeworks, week1-day2
    ATM
*/

const checkBalance = process.argv[2]; 
const isActive = process.argv[3]; 
const balance = Number(process.argv[4]); 

let str;

console.log(checkBalance, isActive, balance);

if(checkBalance === "No"){
	str = "Have a nice day!";
}
else if((isActive==="Yes")&&(balance>0)){
	str = `Your balance is ${balance}.`;
}
else if(isActive!=="Yes"){
	str = "Your account is not active.";
}
else if(balance === 0){
	str = "Your account is empty.";
}
else{
	str = "Your balance is negative.";
}
console.log(str);
