/*  Homeworks, week1-day2
    How many days
*/

const month = Number(process.argv[2]); 

let str;
let days;

console.log(month);

switch(month){
case 1:
case 3:
case 5:
case 7:
case 8:
case 10:
case 12:
	days = 31;
	break;
case 4:
case 6:
case 9:
case 11:
	days = 30;
	break;
case 2:
	days = 28;
	break;
default:
	days = undefined;
	break;
}

if (days!==undefined){
	str = `Month of ${month} has ${days} days.`;
}
else{
	str = "You gave the wrong number.";
}
console.log(str);
