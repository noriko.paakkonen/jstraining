/*  Homeworks, week1-day2
    Largest and smallest
*/

const a = process.argv[2]; //number_1
const b = process.argv[3]; //number_2
const c = process.argv[4]; //number_3

let str;
let max_num;
let min_num;

console.log(a, b, c);

if((a===undefined)||(b===undefined)||(c===undefined)){
	str = "You have to input three numbers.";
}
else if((a===b)&&(b===c)){
	str = "They all are equal.";
}
else{
	max_num = Math.max(a, b, c);
	min_num = Math.min(a, b, c);

	str = `The largest number is ${max_num}, the smallest number is ${min_num}.`;
}

console.log(str);
