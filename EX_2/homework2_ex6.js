/*  Homeworks, week1-day2
    Initial letters
*/

const name_1 = process.argv[2]; 
const name_2 = process.argv[3]; 
const name_3 = process.argv[4]; 

let initialLetters;

console.log(name_1, name_2, name_3);

initialLetters = name_1.substr(0,1) + "." + name_2.substr(0,1) + "." + name_3.substr(0,1);

console.log(initialLetters.toLowerCase());
//console.log(initialLetters);
