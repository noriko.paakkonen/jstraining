/*  Homeworks, week1-day2
    Hello World, part2
*/

const lang = process.argv[2];

let str;

if(lang===undefined){
	console.log("You have to input language code.");
}


switch(lang){
case "fi": //Finnish
	str = "Hei Maailma.";
	break;
case "jp": //Japanese
	str = "こんにちは 世界。";
	break;
case "en": //English
default: //Other languages
	str = "Hello World.";
	break;
}

console.log(str);
