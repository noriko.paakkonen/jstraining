import './App.css';
import React, { useState } from "react"; // instead of Component, import the useState-hook



const App = props => {


  const [text, setText] = useState(props.text);
  const [completeText, setCompleteText] = useState("");

  const handleInputChange = ev => {
    setText(ev.currentTarget.value);
  };

  const handleButton = ev => {
    //ev.preventDefault();
    setCompleteText(text)
  }

  return ( // we just return the JSX to be rendered
    <div>
      <div className="onchange">
        <p>Text will be displayed when input changed.</p>
        {text}
      </div>
      <div className="input">
        <input onChange={handleInputChange} />
        <button onClick={handleButton}>Button</button>
      </div>
      <div className="buttonchange">
        <p>Text will be displayed when button is pressed.</p>
        {completeText}
      </div>
    </div >
  )

}
export default App;
