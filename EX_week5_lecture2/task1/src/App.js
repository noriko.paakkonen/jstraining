import './App.css';

function App() {

  const eulerImg = "https://upload.wikimedia.org/wikipedia/commons/c/c2/Leonhard_Euler_-_edit1.jpg";

  const descriptionForImg = "Portrait by Jakob Emanuel Handmann (1753). Source: Wikipedia";

  const descriptionFromWiki = "Leonhard Euler (15 April 1707 – 18 September 1783) was a Swiss mathematician, physicist, astronomer, geographer, logician and engineer who founded the studies of graph theory and topology and made pioneering and influential discoveries in many other branches of mathematics such as analytic number theory, complex analysis, and infinitesimal calculus. He introduced much of modern mathematical terminology and notation, including the notion of a mathematical function. He is also known for his work in mechanics, fluid dynamics, optics, astronomy and music theory.";

  const linkToWiki = "https://en.wikipedia.org/wiki/Leonhard_Euler";

  const EulerPicture = () => {
    return (
      <div>
        <h1 className="bigtitle">Leonhard Euler</h1>
        <div>
          <img className="img" src={eulerImg} alt="new" />
          <p className="imgdescription">{descriptionForImg}</p>
        </div>
        <figure>
          <blockquote cite={linkToWiki}>
            <p className="description">{descriptionFromWiki}</p>
          </blockquote>
          <figcaption className="quote">—Leonhard Euler page, <a href={linkToWiki}>Wikipedia</a></figcaption>
        </figure>
      </div>
    );

  }
  return (
    <EulerPicture />
  );
}

export default App;
