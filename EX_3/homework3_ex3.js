/*  Homeworks, week2-day2
    Ordinal numbers
*/

const competitors = ["Julia", "Mark", "Spencer", "Ann", "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

let placementArr = []; //for the result
let placement; // number + ordinals

for(let i = 0; i < competitors.length; i++){
	if(i < ordinals.length){
		placement = (i+1) + ordinals[i];
	}
	else{
		// after number 4, always "th"
		placement = (i+1) + ordinals[ordinals.length -1];
	}
	placementArr.push(`${placement} competitor was ${competitors[i]}`);		
	
}

console.log(placementArr);
