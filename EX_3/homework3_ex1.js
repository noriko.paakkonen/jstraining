/*  Homeworks, week2-day2
    Count sheeps
*/

const sheepNum = process.argv[2]; //how many sheeps

let str = "";

if(sheepNum===undefined){
	console.log("You have to input the number of sheep.");
}

for(let i = 1; i <= sheepNum; i++){
	str += `${i} sheep...`;
}

console.log(str);
