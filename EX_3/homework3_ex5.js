/*  Homeworks, week2-day2
    Reverse words in sentence
*/

const originalStr = process.argv[2];
let answerStr ="";

let wordArr = originalStr.split(" "); //put the words as array mode

for(let i = 0; i < wordArr.length; i++){
	//split the word into a character
	let currentWord = wordArr[i].split(""); 
	//reverse the word
	for (let j = currentWord.length -1; j >= 0; j--){
		answerStr += currentWord[j];
	}

	//if the word is not the last, put the space
	if( i !== (wordArr.length -1)){
		answerStr += " ";
	}
}

console.log(answerStr);
