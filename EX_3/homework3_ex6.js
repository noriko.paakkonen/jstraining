/*  Homeworks, week2-day2
    isPalindrome
*/

const inputStr = process.argv[2];
let answerStr; 
let isPalindrome = true;

for(let i = 0; i < (inputStr.length-1)/2; i++){
	// checking the characters from both beginning and end
	if( inputStr[i] !== inputStr[inputStr.length -1 -i]){
		isPalindrome = false;
		break;
	}
}

if(isPalindrome){
	answerStr = `Yes, '${inputStr}' is a palindrome.`;
}
else{
	answerStr = `No, '${inputStr}' is not a palindrome.`;
}
console.log(answerStr);
