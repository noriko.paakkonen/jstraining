/*  Homeworks, week2-day2
    Randomize order of array
*/

const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

let newArr = []; // randomized new array
let random;	// random number
let alreadyUsed = []; //put the numbers which are already used


for(let i = 0; i < array.length; i++){
	random = Math.floor(Math.random() * array.length);
	if(alreadyUsed.includes(random)){
		// if random number is already used, do it again.
		i--;
	}
	else{
		newArr.push(array[random]);
		alreadyUsed.push(random);
	}
}

console.log(newArr);
