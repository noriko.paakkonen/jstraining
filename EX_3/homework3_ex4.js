/*  Homeworks, week2-day2
    Range within
*/

const startNum = Number(process.argv[2]);
const endNum = Number(process.argv[3]);

let arr = [];

if(isNaN(startNum)||isNaN(endNum)){
	console.log("You have to input two numbers.");
}
else if(startNum < endNum){ //ascending
	for(let i = startNum; i <= endNum; i++){
		arr.push(i);
	}
}
else if(startNum > endNum){ //descending
	for(let i = startNum; i >= endNum; i--){
		arr.push(i);
	}
}
else{ // startNum === endNum
	arr.push(startNum);
}

console.log(arr);
