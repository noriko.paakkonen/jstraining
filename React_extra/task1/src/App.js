import './App.css';
import { useState } from "react";

function App() {
  const [inputTodo, setInputTodo] = useState(""); //new ToDo to add
  const [todoList, setTodoList] = useState([]); // List of To do

  const handleInputTodo = (e) => {
    setInputTodo(e.target.value);
  }

  const handleAdd = (e) => {
    e.preventDefault()
    if (inputTodo === "") { //no input => no need to add.
      return;
    }
    setTodoList(arr => [...arr, { name: inputTodo, isCompleted: false }]);
    setInputTodo("");
  }

  const handleDelete = (index) => {
    const newTodoList = [...todoList];
    newTodoList.splice(index, 1); // Delete the item from the To Do list
    setTodoList(newTodoList);
  }

  const handleUpdateTask = index => {
    // Update the task if it is completed or not
    let newTodoList = todoList.map((todo, todoIndex) => {
      if (todoIndex === index) {
        todo.isCompleted = !todo.isCompleted; 
      }
      return todo;
    })
    setTodoList(newTodoList);
  }

  const ToDoList = () => {
    return (
      <div className="list">
        <h3>List of to do</h3>
        {todoList.map((todo, index) => {
          return (
            <div key={index} style={{ textDecoration: todo.isCompleted ? 'line-through' : 'none' }}>
              <input type="checkbox" onChange={() => handleUpdateTask(index)} checked={todo.isCompleted} />
              <span> {todo.name} </span>
              <button onClick={() => handleDelete(index)}>Delete</button>
            </div>
          )
        })}
      </div>
    );
  }

  return (
    <div className="todolist">
      <h1>Your ToDo List</h1>
      <form onSubmit={handleAdd}>
        <input placeholder="Add new ToDo here" onChange={handleInputTodo} value={inputTodo} />
        <button type="submit">Add</button>
      </form>
      {todoList.length !== 0 ? <ToDoList /> : null}
    </div>
  );

}
export default App;
